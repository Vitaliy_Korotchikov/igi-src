package by.gsu.igi.students.VitalyKorotchikov.lab3;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Evgeniy Myslovets.
 */
public class ArrayOperationsDemo {

    private static int findMax(int[] numbers) {
        int max = numbers[0];

        for (int number : numbers) {
            if (number > max) {
                max = number;
            }
        }

        return max;
    }

    private static int findMin(int[] numbers) {
        int min = numbers[0];

        for (int number : numbers) {
            if (number < min) {
                min = number;
            }
        }

        return min;
    }

    private static double calculateAverage(int[] numbers) {
        int average = 0;

        for (int number : numbers) {
            average += number;

        }
        return average / (double) numbers.length;
    }

    private static int product(int[] numbers) {
        int product = 1;

        for (int number : numbers) {
            product *= number;
        }

        return product;
    }

    private static int sum(int[] numbers) {
        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }

        return sum;
    }

    private static int difference(int[] numbers) {
        int diff = 0;

        for (int number : numbers) {
            diff -= number;
        }

        return diff;
    }

    public static void main(String[] args) {
        int[] numbers = pastArray(args);
        processArray(numbers);


    }
    private static int[] pastArray (String[] str) {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("1- консоль ; 2 - через параметр ; 3 - random числа");

            int k = scanner.nextInt();

            switch (k) {
                case 2:
                    if (str.length != 0) try {
                      return  readArray(str);
                    } catch (NumberFormatException e ) {
                        System.out.println("Введено неправльное значение");
                        break;
                    }catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println("Командная строка пуста");
                        break;
                    }
                    else try {
                      return  readArray();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println("Командная строка пуста");
                        break;
                    } catch (NumberFormatException e ) {
                        System.out.println("Введено неправльное значение");
                        break;
                    }catch (InputMismatchException e) {
                        System.out.println("Введено неправльное значение");
                        break;
                    }
                case 1:
                    try {
                    return    readArray();
                    } catch (InputMismatchException e) {
                        System.out.println("Введено неправльное значение");
                        break;
                    } catch (NumberFormatException e ) {
                        System.out.println("Введено неправльное значение");
                        break;
                    }

                case 3:{
                   return readRandomArray();}
                default: {
                    System.out.println("Введено неверное число");
                    break;
                }
            }

        }
    }

    private static int[] readArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество элементов:");
        int size = scanner.nextInt();

        int[] numbers = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Введите " + (i + 1) + "-й элемент:");
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }
    private static int[] readArray(String[] str) {
        System.out.println("Длинна массива: " + (str.length));
        int numbers[] = new int[str.length];
        for (int i = 0; i < str.length; i++) {
            numbers[i] = Integer.parseInt(str[i]);
        }

        return numbers;
    }

    private static int[] readRandomArray() {

        int size = ((int) (Math.random() * 10)) + 1;

        int[] numbers = new int[size];
        for (int i = 0; i < size; i++) {
            numbers[i] = (int) (Math.random() * 10);
            System.out.println((i + 1) + " элемент = " + numbers[i]);
        }

        return numbers;
    }


    private static void processArray(int[] numbers) {
        int max = findMax(numbers);
        System.out.println("Максимальный элемент: " + max);

        int min = findMin(numbers);
        System.out.println("Минимальный элемент: " + min);

        double average = calculateAverage(numbers);
        System.out.println("Среднее значение: " + average);

        int product = product(numbers);
        System.out.println("Произведение элементов: " + product);

        int sum = sum(numbers);
        System.out.println("Сумма элементов: " + sum);

        int difference = difference(numbers);
        System.out.println("Разность элементов: " + difference);
    }
}
