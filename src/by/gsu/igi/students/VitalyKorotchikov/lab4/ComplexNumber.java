package by.gsu.igi.students.VitalyKorotchikov.lab4;

/**
 * Created by Vitalya on 10.11.2016.
 */

public class ComplexNumber {
    private double real;
    private double imaginaries;
    private double module;
    private double corner;

    public ComplexNumber(double real, double imaginaries) {
        this.real = real;
        this.imaginaries = imaginaries;
        module = Math.sqrt(real * real + imaginaries * imaginaries);
        fixModuleAndCorner(real, imaginaries);
    }

    public double getReal() {
        return real;
    }
    public double getImaginaries() {
        return imaginaries;
    }

    public ComplexNumber add(ComplexNumber complex) {
        return new ComplexNumber(real + complex.getReal(), imaginaries + complex.getImaginaries());
    }
    public ComplexNumber substract(ComplexNumber complex) {
        return new ComplexNumber(real - complex.getReal(), imaginaries - complex.getImaginaries());
    }
    public ComplexNumber multiply(ComplexNumber complex) {
        double real = this.real * complex.getReal() - this.imaginaries * complex.getImaginaries();
        double imaginaries = real * complex.getImaginaries() + this.imaginaries * complex.getReal();
        return new ComplexNumber(real, imaginaries);
    }
    public ComplexNumber divide(ComplexNumber q) {
        double real = (this.real * q.getReal() + this.imaginaries * q.getImaginaries()) / (this.real * this.real + q.getReal() * q.getReal());
        double imaginaries = (this.imaginaries * q.getReal() - this.real * q.getImaginaries()) / (this.real * this.real + q.getReal() * q.getReal());
        return new ComplexNumber(real, imaginaries);
    }
    @Override
    public String toString() {
        String s = showAlgebraic() + "\n" + showTrigonometric();
        return s;
    }
    public String showAlgebraic() {
        return "z = " + real + " + " + imaginaries + "i";
    }
    public String showTrigonometric() {
        return "z = " + module + "*(cos(" + corner + ") + sin(" + corner + "))";
    }
    private void fixModuleAndCorner(double real, double imaginaries) {
        if (real != 0) {
            corner = Math.atan(imaginaries / real);
        } else {
            if (imaginaries != 0) {
                corner = Math.signum(real) * (Math.PI / 2);
            } else {
                corner = 0;
            }
        }
    }

    public static void main(String[] args) {
        ComplexNumber x = new ComplexNumber(1, 3);
        ComplexNumber y = new ComplexNumber(2, 2);
        System.out.println("sum:");
        System.out.println(x.add(y));
        System.out.println("difference:");
        System.out.println(x.substract(y));
        System.out.println("multiplication:");
        System.out.println(x.multiply(y));
        System.out.println("division:");
        System.out.println(x.divide(y));
    }
}