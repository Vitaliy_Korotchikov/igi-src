package by.gsu.igi.students.VitalyKorotchikov.lab5;

/**
 * Created by Vitalya on 21.12.2016.
 */
public interface Speakable {
    void speak();
}
