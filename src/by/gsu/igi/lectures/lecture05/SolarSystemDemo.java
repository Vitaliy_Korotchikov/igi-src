package by.gsu.igi.lectures.lecture05;

/**
 * @author Evgeniy Myslovets
 * @date 02.03.14
 */
public class SolarSystemDemo {

    public static void main(String[] args) {
        Planet planet = Planet.MARS;
        double mass = 42;
        System.out.println("На планете " + planet + " я вешу " + planet.surfaceWeight(mass) / 10 + " кг");
    }
}
